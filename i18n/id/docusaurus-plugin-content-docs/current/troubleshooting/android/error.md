---
title: "Error Codes"
sidebar_position: 2
description: "List of common error codes"
---
# 4214

Jika Anda mencoba terhubung ke Server Resmi, tidak diizinkan untuk terhubung ke Server Resmi menggunakan apk launcher mod karena libil2cpp.so di-patch.

Jika Anda mencoba terhubung ke Private Server, itu karena proxy gagal mengubah URL ke Private Server. Anda dapat mencoba membuka kembali game dan mencoba lagi.

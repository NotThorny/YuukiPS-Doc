---
sidebar_position: 1
---

# Introduction

Welcome to YuukiPS. Before you connect to our server, there are a few things you need to know.

## Genshin Impact Support

- GIO 3.2.0 is available for PC and Android.
- GC 4.0.0 / 4.0.1 is available for PC and Android.
- GC 4.5.0 is available for PC and Android.

## Star Rail Support

- LC 2.2.0 is available for PC and Android.

## Genshin Impact Requirements

### PC

- Patch RSAPatch for version 3.3+.
- Patch UserAssembly for version 3.1-3.2.
- Patch Metadata for version 2.8-3.0.

(Tips: Don't mix, just use one of them.)

### Android & iOS

- Proxy Launcher 1.0-2.7.
- Proxy Launcher + RSA modification + SSL bypass 2.8+.

## Star Rail Requirements

NOTHING / DOESN'T NEED

### Note

- All servers are free. If you pay someone to use this server, you are being scammed, so be careful.
- Make sure to log in at least once every month to prevent your private server account from being deleted.
- Maintaining servers costs a lot of money. If you can afford it, consider sponsoring to keep the servers up and everyone happy.
- Don't share your username with anyone if you don't want to be trolled or have your items deleted and quest reset by others.

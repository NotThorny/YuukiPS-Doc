---
sidebar_position: 4
--- 

# Cultivation

Panduan untuk menggunakan Cultivation untuk terhubung ke Private Server

:::info
Mulai dari versi 2.7 dan seterusnya [Memerlukan Patch](/docs/intro)
:::

## Mengunduh

1. Dapatkan versi terbaru dari [sini](https://github.com/Grasscutters/Cultivation/releases).

2. Unduh file `.zip` atau `.msi` (Disarankan).
![img](./img/download-culti.png)

## Menjalankan

1. Jalankan installer jika Anda mengunduh `.msi`, ekstrak zip ke folder jika Anda menginstal `.zip`, setelah itu jalankan `Cultivation.exe` dengan hak administrator.

2. Jika Anda melihat peringatan keamanan, klik Ya. Jika Anda tidak mempercayai CA, proxy launcher tidak akan berfungsi. Anda dapat menginstal sertifikat proxy di pengaturan launcher "**Install Proxy Certificate**" (Kecuali jika Anda menggunakan daemon proxy terpisah, Anda dapat mengaturnya di pengaturan launcher dengan mematikan "**Use Internal Proxy**").

    ![img](./img/trust-cert-culti.png)

3. Tentukan jalur game pada pengaturan launcher (Cari `GenshinImpact.exe`)
![img](./img/game-path-culti.png)

4. Aktifkan "**Connect via Grasscutter**" dan "**use HTTPS**\*", lalu masukkan alamat server `ps.yuuki.me` dan nomor port (443 adalah nomor port default untuk grasscutter), setelah itu klik Launch.

    *Nonaktifkan hanya jika tidak ada Sertifikat SSL yang terpasang pada domain

    ![img](./img/launch-game-culti.png)

    Izinkan akses firewall

    ![img](./img/allow-firewall-culti.png)

5. Tunggu hingga game terbuka.

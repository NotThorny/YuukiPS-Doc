export interface returnApplyTheCommand {
    message: string;
    retcode: number;
}

export interface Account {
    data: Account_Data;
    message: string;
    retcode: number;
}

export interface Account_Data {
    account: Account;
    player: Player[];
}

export interface Account {
    uid: string;
    locale: string;
    code: string;
}

export interface Player {
    data: PlayerData;
    name: string;
    title: string;
    type_game: number;
    api_game: number;
}

export interface PlayerData {
    message: string;
    retcode: number;
    data: DataData | null;
}

export interface DataData {
    _id: number;
    accountId: string;
    nickname: string;
    signature: string;
}
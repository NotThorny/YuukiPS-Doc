---
sidebar_position: 1
title: "FAQ"
---

# FAQ

:::info
Daftar Pertanyaan yang Sering Diajukan (FAQ) untuk Android.
:::

## Mengapa terhenti di "Preparing to load data 45%"?

Untuk memperbaikinya, Anda harus memiliki data game terlebih dahulu.<br/>
Buka `/sdcard/Android/data`<br/>
Jika Anda menggunakan V1, maka ubah namanya menjadi `com.z3ro.YuukiPS`<br/>
Jika Anda menggunakan V2, maka ubah namanya menjadi `com.miHoYo.GenshinImpact`

Jika masih tidak berfungsi meskipun sudah diubah namanya menjadi `com.z3ro.YuukiPS` atau `com.miHoYo.GenshinImpact`, maka cobalah mengunduh Data Game dari Server Resmi dan ubah namanya menjadi `com.z3ro.YuukiPS` atau `com.miHoYo.GenshinImpact`, lalu coba lagi.

<iframe width="420" height="215" src="https://www.youtube.com/embed/orOCazI4uVA" title="Tutorial" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Apa perbedaan antara Versi 1 dan 2?

Versi 1 dapat terhubung ke Localhost, YuukiPS, dan server lainnya.<br/>
Versi 2 dapat terhubung ke LocalHost, Server Resmi, YuukiPS dan lebih baik untuk memperbaiki masalah waktu loading (0kb).

## Apa perbedaan antara GIO dan GC?

GIO adalah klon dari semua fungsi dari server resmi yang berarti semua fungsi berfungsi 100% tanpa bug dan semua quest berfungsi tetapi hanya dapat dijalankan pada Versi 3.2.

GC adalah emulator server yang dapat dijalankan pada versi terbaru dan versi beta, tetapi banyak fungsi yang tidak berfungsi, hanya saja perintah lebih mudah karena dapat diakses melalui Ayaka Chat.

## Gagal memuat il2cpp

Perbaiki dengan menghapus folder "il2cpp" di Folder Data Game dan masuk ke dalam game, game akan terbuka tanpa kesalahan.

## Error 4214

Jika Anda masuk ke server resmi, sebenarnya tidak diizinkan untuk terhubung ke server resmi menggunakan apk launcher mod karena berisiko.

Jika Anda mencoba terhubung ke Private Server dan mendapatkan pesan kesalahan ini, maka proxy gagal mengubah domain atau server ke Private Server. Anda dapat mencoba membuka kembali game dan mencoba lagi.

## Android 13 rename

### ADB

Anda dapat mencoba mengubah nama folder `com.miHoYo.GenshinImpact` menjadi `com.z3ro.YuukiPS` dengan ADB.<br/>

```bash
adb shell mv /sdcard/Android/data/com.miHoYo.GenshinImpact /sdcard/Android/data/com.z3ro.YuukiPS
```

![adb](./gif/adb.gif)

### Files

Anda dapat mencoba mengubah nama folder `com.miHoYo.GenshinImpact` menjadi `com.z3ro.YuukiPS` dengan Aplikasi Files.<br/>
Unduh [Files](https://play.google.com/store/apps/details?id=com.marc.files)

![files](./gif/files.gif)

### Still not working?

Jika Anda masih tidak dapat mengubah nama folder, itu berarti sistem Anda tidak diizinkan untuk mengubah nama folder di `/sdcard/Android/data/`<br/>
Bahkan dengan ADB atau Aplikasi Files, Anda tidak dapat mengubah nama folder.<br/>
Namun, solusi terakhir adalah melakukan root pada ponsel Anda agar dapat mengubah nama folder.<br/>
Jika Anda tidak ingin melakukan root pada ponsel Anda, maka cobalah menggunakan V2 daripada V1. Lihat [Cara menginstal V2](/docs/tutorial-connect/android/install#download-from-official-server)

## Apakah Root Diperlukan?

Tidak, Root tidak diperlukan untuk mod ini.

## Dapatkah Saya Menggunakan VPN untuk Mod Ini?

Ya, Anda dapat menggunakan VPN untuk mod ini. Mod ini tidak menggunakan proxy yang memerlukan penggunaan Proxy (127.0.0.1:PORT) untuk terhubung ke Private Server.

## Bagaimana Cara Kembali ke Server Resmi?

### YuukiPS V1

1. Buka `/sdcard/Android/data` dan ubah nama folder `com.z3ro.YuukiPS` menjadi `com.miHoYo.GenshinImpact`.
2. Jika Anda sudah menghapus `Genshin Impact`, unduh dari Play Store.
3. Buka `Genshin Impact`.

### YuukiPS V2

#### Instalasi No Module

1. Anda dapat mengunduh [No Module](/docs/tutorial-connect/android/download#no-module)
2. Dan instal `Genshin Impact` untuk bermain di Server Resmi.
3. Jika Anda ingin bermain di Private Server lagi, cukup unduh [Mod APK](/docs/tutorial-connect/android/download#mod-apk) dan instal untuk bermain di Private Server.

#### Instalasi dari Play Store

1. Anda perlu mencadangkan data game Anda terlebih dahulu sebelum bermain di Server Resmi.
2. Buka `/sdcard/Android/data` dan ubah nama folder `com.miHoYo.GenshinImpact` menjadi `com.miHoYo.GenshinImpact.bak`.
3. Kemudian hapus `Genshin Impact`.
4. Dan instal `Genshin Impact` dari Play Store.
5. Ubah kembali data game yang sudah Anda cadangkan `com.miHoYo.GenshinImpact.bak` menjadi `com.miHoYo.GenshinImpact`.

## Apakah mungkin untuk bermain localhost di Android?

Ya, mungkin untuk bermain localhost di Android.
Jika Anda ingin meng-host Grasscutter di PC Anda dan memainkannya di Android Anda, Anda dapat memeriksa [DockerGS](https://github.com/YuukiPS/DockerGS)

Jika Anda ingin meng-host Grasscutter di Android Anda, Anda dapat memeriksa [GCAndroid](https://github.com/Score-Inc/GCAndroid)

Setelah Anda meng-host Grasscutter di PC atau Android Anda, Anda dapat mengedit file bernama "server.json" di Folder Data Game `/sdcard/Android/data/com.z3ro.YuukiPS/files` atau `/sdcard/Download/YuukiPS`, dan mengubah nilai "server" menjadi "127.0.0.1" atau dengan IP Anda. Contoh: `"server": "https://127.0.0.1:54321"`<br/>
Untuk mendapatkan PORT, Anda dapat memeriksa di `config.json` di folder Grasscutter setelah Anda menjalankan Grasscutter.

```json filename="config.json" {3}
"http": {
  "bindAddress": "0.0.0.0",
  "bindPort": 54321,
  "accessAddress": "127.0.0.1",
  "accessPort": 0,
  "encryption": {
    "useEncryption": true,
    "useInRouting": true,
    "keystore": "./keystore.p12",
    "keystorePassword": "123456"
  }
},
```

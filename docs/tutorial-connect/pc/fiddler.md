---
sidebar_position: 3
---

# Fiddler Classic

Guide to use Fiddler for connecting to Private Server
:::info
Genshin Impact: version 2.7 and up [Requires Patch](/docs/intro)<br/>
Star Rail: there is no need for any patch, only a proxy.
:::

## Download

- Download and install Fiddler Classic from [here](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Tool/FiddlerSetup.exe).

## Running

1. Run Fiddler Classic with administrator

2. Trust the Fiddler Classic Root Certificate. If you did not trust it the proxy won't work. You can set this**\***

   ![img](./img/cert-warning.png)

   > Tools -> Options -> HTTPS -> Actions -> Trust Root Certificate -> Toggle Decrypt HTTPS traffic on

3. Paste this to your FiddlerScript tab.

   ```cs
   import System;
   import System.Windows.Forms;
   import Fiddler;
   import System.Text.RegularExpressions;
   class Handlers
   {
       static function OnBeforeRequest(oS: Session) {
           if(
               oS.host.EndsWith(".yuanshen.com") ||
               oS.host.EndsWith(".hoyoverse.com") ||
               oS.host.EndsWith(".mihoyo.com") ||
               oS.host.EndsWith(".zenlesszonezero.com") ||
               oS.host.EndsWith(".honkaiimpact3.com") ||
               oS.host.EndsWith(".bhsr.com") ||
               oS.host.EndsWith(".starrails.com") ||
               oS.uriContains("http://overseauspider.yuanshen.com:8888/log")
            ) {
                 oS.host = "ps.yuuki.me";
            }
       }
   };
   ```

   ![img](./img/fiddler-script.png)

4. Save the Script

   ![img](./img/save-script.png)

5. Run the game

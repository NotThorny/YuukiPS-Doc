import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {FaCopy} from 'react-icons/fa';
import {AiOutlineCheck} from 'react-icons/ai'

interface ListData {
    name: string;
    description: string;
    command: string;
    example: {
        command: string;
        explain: string;
    } | null;
    args: [key: string] | [];
}

const CodeBlock = ({ code }) => {
    const [isCopied, setIsCopied] = useState(false);

    const copyCodeToClipboard = () => {
        navigator.clipboard.writeText(code).then(() => {
            setIsCopied(true);
            setTimeout(() => {
                setIsCopied(false);
            }, 2000);
        });
    };

    return (
        <div style={{ position: 'relative' }}>
            <pre style={{ padding: '1rem' }}>
                {code}
            </pre>
            <button
                onClick={copyCodeToClipboard}
                style={{
                    position: 'absolute',
                    top: '0.5rem',
                    right: '0.5rem',
                    padding: '0.5rem',
                    border: '1px solid #333',
                    background: 'none',
                    cursor: 'pointer',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: '0.25rem',
                    borderColor: 'blue',
                }}
                className={isCopied ? 'copied' : ''}
            >
                {isCopied ? (
                    <AiOutlineCheck />
                ) : (
                    <FaCopy />
                )}
            </button>
        </div>
    );
};

CodeBlock.propTypes = {
    code: PropTypes.string.isRequired,
};

function calculateLevenshteinDistance(str1: string, str2: string): number {
    const m = str1.length;
    const n = str2.length;
    const dp: number[][] = [];

    for (let i = 0; i <= m; i++) {
        dp[i] = new Array<number>(n + 1);
        dp[i][0] = i;
    }

    for (let j = 1; j <= n; j++) {
        dp[0][j] = j;
    }

    for (let i = 1; i <= m; i++) {
        for (let j = 1; j <= n; j++) {
            if (str1[i - 1] === str2[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1];
            } else {
                dp[i][j] = 1 + Math.min(dp[i - 1][j - 1], dp[i][j - 1], dp[i - 1][j]);
            }
        }
    }

    return dp[m][n];
}

const searchFunction = ({ data } ) => {
    const [searchQuery, setSearchQuery] = useState('');

    const filteredData = data.filter((item: object) => 
        Object.values(item).some((value) => {
            if (searchQuery === '') return '';
            if (typeof value === 'string') {
                return value.toLowerCase().includes(searchQuery.toLowerCase());
            }
        })
    );

    const renderResult = () => {
        const similarName = data.filter((item: ListData) => {
            const distance = calculateLevenshteinDistance(searchQuery, item.name);
            if (distance <= 5) {
                return item.name;
            }
        });


        if (searchQuery !== '' && filteredData.length === 0) {
            return (
                <p
                    style={{
                        color: 'white',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        transform: 'translateY(-50%)',
                    }}
                >
                    {similarName.length > 0 ? (
                        <span>
                            Did you mean <button
                                style={{
                                    color: 'white',
                                    fontSize: '20px',
                                    fontWeight: 'bold',
                                    backgroundColor: 'transparent',
                                    border: 'none',
                                    cursor: 'pointer',

                                }}
                                onClick={() => setSearchQuery(similarName[0]?.name)}
                            >
                                {similarName[0]?.name}
                            </button>?
                        </span>
                    ) : (
                        <span>
                            No results found for <span style={{ color: 'red' }}>{searchQuery}</span>
                        </span>
                    )}
                </p>
            );
        }
        return null;
    };

    return (
        <div>
            <input
                type="text"
                value={searchQuery}
                onChange={(event) => setSearchQuery(event.target.value)}
                onKeyDown={(event) => {
                    if (event.key === 'Escape') {
                        setSearchQuery('');
                    }
                }}
                style={{
                    width: '100%',
                    height: '50px',
                    fontSize: '20px',
                    padding: '10px',
                    boxSizing: 'border-box',
                    border: '1px solid black',
                    marginBottom: '10px',
                    borderColor: 'blueviolet',
                    borderRadius: '10px',
                }}
                placeholder="Search Command..."
            />
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    overflowY: 'scroll',
                    maxHeight: '500px',
                    paddingRight: '5px',
                }}
            >
                {filteredData.map((item: ListData) => (
                    <div
                        key={`${item.name}-${item.description}`}
                        style={{
                            border: '1px solid black',
                            padding: '10px',
                            margin: '10px',
                            borderColor: 'blue',
                            borderRadius: '10px',
                            width: '100%',
                            height: '100%',
                            backgroundColor: 'rgba(0, 0, 0, 0.8)',
                            justifyContent: 'center',
                        }}
                    >
                        <p
                            style={{
                                color: 'white',
                                fontSize: '20px',
                                fontWeight: 'bold',
                            }}
                        >
                            {item.name}
                        </p>
                        <hr />
                        <p
                            style={{
                                color: 'white',
                                fontSize: '20px',
                                fontWeight: 'bold',
                            }}
                        >
                            {item.description}
                        </p>
                        <hr />
                        <h2>Usage</h2>
                        <CodeBlock code={`${item.command}${item.args.map((arg, index) => {
                            const space = ' '.repeat(index === 0 ? 1 : 0);
                            return `${space}${Object.keys(arg)[0]}`;
                        }).join(' ')}`} />
                            {item.args.length > 0 ? (
                            <div>
                                <hr />
                                <h2>Explanation</h2>
                                {item.args.map((arg) => {
                                    const key = Object.keys(arg)[0];
                                    const value = Object.values(arg)[0];

                                    return (
                                        <div key={`${key}-${value}`}>
                                            <p>
                                                <code>{key}</code>: {value as React.ReactNode}
                                            </p>
                                        </div>
                                    )
                                })}
                            </div>
                        ) : null}
                    </div>
                ))}
            </div>
            {renderResult()}
            {searchQuery !== '' && filteredData.length > 0 ? (
                <p
                    style={{
                        color: 'white',
                        fontSize: '20px',
                        fontWeight: 'bold',
                        transform: 'translateY(-50%)',
                    }}
                >
                    {filteredData.length} result{filteredData.length > 1 ? 's' : ''} found for <span style={{ color: 'red' }}>{searchQuery}</span>
                </p>
            ) : null}
        </div>
    )
};

export default searchFunction;

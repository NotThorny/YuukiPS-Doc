import React, { useState, useEffect } from 'react';
import './styles.css'
import { Root, Root2 } from '../../types/contributor'

const Page = () => {
    const [contributor, setContributor] = useState<Root2>()

    useEffect(() => {
        const load = async () => {
            const fetchJSON = await fetch('/contributor.json');
            const convertToJson: Root = await fetchJSON.json();
            convertToJson.forEach((structure) => {
                if (structure.website_url === window.location.pathname) {
                    setContributor(structure);
                }
            })
        }
        load()
    }, [])

    return (
        <>
            {contributor && contributor.contributors.map((contributors) => (
                    <a href={`https://discord.com/users/${contributors.id}`}>
                        <div className={'contributor'} title={contributors.nickname}>
                            <img className={'profile-picture'} src={contributors.profile_picture_url} alt={`ID ${contributors.id}`}/>
                        </div>
                    </a>
                ))
            }
        </>
    )
}

export default Page
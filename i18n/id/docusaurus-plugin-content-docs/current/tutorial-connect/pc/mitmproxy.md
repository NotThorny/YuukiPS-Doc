---
sidebar_position: 4
---

# mitmproxy

Panduan untuk menggunakan mitmproxy untuk terhubung ke Private Server

(Untuk Pengguna Lanjutan)

:::info
Mulai dari versi 2.7 dan seterusnya [Memerlukan Patch](/docs/intro)
:::

## Download

- Download mitmproxy from [here](https://mitmproxy.org/)

## Instalasi

1. Instal mitmproxy dan jalankan sebagai _administrator_\* (*opsional jika proxy tidak berfungsi dengan baik)

    Anda akan melihat konsol yang berjalan

    ![img](./img/console-mitm.png)

    Dan izinkan akses firewall (Jika tidak diizinkan, maka tidak akan berfungsi)

    :::note
    Jika aplikasi tidak berjalan, silakan lihat [troubleshooting](/docs/troubleshooting/pc/mitmproxy-troubleshoot)
    :::

2. Ubah proxy windows menjadi `127.0.0.1` dan `8080` (port default untuk mitm)

    Buka pengaturan windows -> Proxy
    ![img](./img/proxy-win.png)

    Perhatikan bahwa jika Anda memiliki proxy lain yang berjalan, ini tidak akan berfungsi, silakan gunakan salah satu pilihan Anda saja
    ![img](./img/set-proxy-win.png)

    Setelah itu, Anda akan melihat aliran lalu lintas
    ![img](./img/traffic-flow.png)

3. Kunjungi [mitm.it](https://mitm.it) dan unduh sertifikat CA mitmproxy

    Situs ini hanya dapat diakses jika Anda menjalankan mitm dan mengonfigurasi proxy, jika Anda tidak dapat mengakses situs, pastikan Anda telah melakukan langkah 1 - 3
    ![img](./img/down-cert-mitm.png)

    Instal sertifikat
    > Anda dapat mencoba instalasi otomatis atau manual jika Anda tidak tahu caranya

    Jika Anda melihat jendela "Peringatan Keamanan", klik Ya (Jika tidak, maka tidak akan berfungsi)

    ![img](./img/trust-cert-mitm.png)

4. Anda sudah siap, sekarang Anda dapat menutup mitmproxy dan mematikan proxy windows Anda (Jika Anda ingin melanjutkan nanti, jika tidak Anda dapat melewati **Mematikan proxy windows**)*

    :::note
    Anda tidak akan dapat terhubung ke internet kecuali Anda mematikan proxy. Lihat [Troubleshooting](/docs/troubleshooting/pc/mitmproxy-troubleshoot#my-pc-cant-connect-to-the-internet) untuk informasi lebih lanjut.
    :::

## Menjalankan

1. Buat folder (Opsional)

2. Buat file bernama `proxy_config.py` dan tempelkan kode berikut :

    ```py
    import os

    # This can also be replaced with another IP address.
    USE_SSL = True
    REMOTE_HOST = "login.yuuki.me"
    REMOTE_PORT = 443
    
    if os.getenv('MITM_REMOTE_HOST') != None:
        REMOTE_HOST = os.getenv('MITM_REMOTE_HOST')
    if os.getenv('MITM_REMOTE_PORT') != None:
        REMOTE_PORT = int(os.getenv('MITM_REMOTE_PORT'))
    if os.getenv('MITM_USE_SSL') != None:
        USE_SSL = bool(os.getenv('MITM_USE_SSL'))

    print('MITM Remote Host: ' + REMOTE_HOST)
    print('MITM Remote Port: ' + str(REMOTE_PORT))
    print('MITM Use SSL ' + str(USE_SSL))
    ```

3. Buat file baru bernama `proxy.py` dan tempelkan kode berikut :

    ```py
    import collections
    import random
    from mitmproxy import http, connection, ctx, tls
    from abc import ABC, abstractmethod
    from enum import Enum
    from mitmproxy.utils import human
    from proxy_config import USE_SSL
    from proxy_config import REMOTE_HOST
    from proxy_config import REMOTE_PORT

    class MlgmXyysd_Animation_Company_Proxy:

        LIST_DOMAINS = [
            "api-os-takumi.mihoyo.com",
            "hk4e-api-os-static.mihoyo.com",
            "hk4e-sdk-os.mihoyo.com",
            "dispatchosglobal.yuanshen.com",
            "osusadispatch.yuanshen.com",
            "account.mihoyo.com",
            "log-upload-os.mihoyo.com",
            "dispatchcntest.yuanshen.com",
            "devlog-upload.mihoyo.com",
            "webstatic.mihoyo.com",
            "log-upload.mihoyo.com",
            "hk4e-sdk.mihoyo.com",
            "api-beta-sdk.mihoyo.com",
            "api-beta-sdk-os.mihoyo.com",
            "cnbeta01dispatch.yuanshen.com",
            "dispatchcnglobal.yuanshen.com",
            "cnbeta02dispatch.yuanshen.com",
            "sdk-os-static.mihoyo.com",
            "webstatic-sea.mihoyo.com",
            "webstatic-sea.hoyoverse.com",
            "hk4e-sdk-os-static.hoyoverse.com",
            "sdk-os-static.hoyoverse.com",
            "api-account-os.hoyoverse.com",
            "hk4e-sdk-os.hoyoverse.com",
            "overseauspider.yuanshen.com",
            "gameapi-account.mihoyo.com",
            "minor-api.mihoyo.com",
            "public-data-api.mihoyo.com",
            "uspider.yuanshen.com",
            "sdk-static.mihoyo.com",
            "abtest-api-data-sg.hoyoverse.com",
            "log-upload-os.hoyoverse.com"
        ]

        def request(self, flow: http.HTTPFlow) -> None:
            if flow.request.host in self.LIST_DOMAINS:
                if USE_SSL:
                    flow.request.scheme = "https"
                else:
                    flow.request.scheme = "http"
                flow.request.host = REMOTE_HOST
                flow.request.port = REMOTE_PORT

    class InterceptionResult(Enum):
        SUCCESS = 1
        FAILURE = 2
        SKIPPED = 3

    class TlsStrategy(ABC):
        def __init__(self):
            self.history = collections.defaultdict(lambda: collections.deque(maxlen=200))

        @abstractmethod
        def should_intercept(self, server_address: connection.Address) -> bool:
            raise NotImplementedError()

        def record_success(self, server_address):
            self.history[server_address].append(InterceptionResult.SUCCESS)

        def record_failure(self, server_address):
            self.history[server_address].append(InterceptionResult.FAILURE)

        def record_skipped(self, server_address):
            self.history[server_address].append(InterceptionResult.SKIPPED)

    class ConservativeStrategy(TlsStrategy):
        def should_intercept(self, server_address: connection.Address) -> bool:
            return InterceptionResult.FAILURE not in self.history[server_address]

    class ProbabilisticStrategy(TlsStrategy):
        def __init__(self, p: float):
            self.p = p
            super().__init__()

        def should_intercept(self, server_address: connection.Address) -> bool:
            return random.uniform(0, 1) < self.p

    class MaybeTls:
        strategy: TlsStrategy

        def load(self, l):
            l.add_option(
                "tls_strategy", int, 0,
                "TLS passthrough strategy. If set to 0, connections will be passed through after the first unsuccessful "
                "handshake. If set to 0 < p <= 100, connections with be passed through with probability p.",
            )

        def configure(self, updated):
            if "tls_strategy" not in updated:
                return
            if ctx.options.tls_strategy > 0:
                self.strategy = ProbabilisticStrategy(ctx.options.tls_strategy / 100)
            else:
                self.strategy = ConservativeStrategy()

        def tls_clienthello(self, data: tls.ClientHelloData):
            server_address = data.context.server.peername
            if not self.strategy.should_intercept(server_address):
                ctx.log(f"TLS passthrough: {human.format_address(server_address)}.")
                data.ignore_connection = True
                self.strategy.record_skipped(server_address)

        def tls_established_client(self, data: tls.TlsData):
            server_address = data.context.server.peername
            ctx.log(f"TLS handshake successful: {human.format_address(server_address)}")
            self.strategy.record_success(server_address)

        def tls_failed_client(self, data: tls.TlsData):
            server_address = data.context.server.peername
            ctx.log(f"TLS handshake failed: {human.format_address(server_address)}")
            self.strategy.record_failure(server_address)
            
    addons = [
        MlgmXyysd_Animation_Company_Proxy(),
        MaybeTls()
    ]
    ```

4. Pastikan kedua file `proxy_config.py` dan `proxy.py` berada di lokasi yang sama.

5. Sekarang buka command prompt / terminal di lokasi kedua file tersebut berada

    ![img](./img/example-folder.png)

    dan ketikkan

    ```bash
    mitmdump.exe -s proxy.py -k
    ```

   Kemudian lakukan langkah 2 dari [Instalasi](#instalasi)

6. Jalankan game

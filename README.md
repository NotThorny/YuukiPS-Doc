# YuukiPS Doc

This is for the Yuuki server for documentation purposes at [doc.yuuki.me](https://doc.yuuki.me).

## Getting Started

First, run development server:

```bash
npm update
npm run start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

* for [translation](https://gist.github.com/FedericoTartarini/ffbf13def91ae2012569a805b7c56900)

```bash
npx docusaurus write-translations --locale id
npm run start -- --locale id
mkdir -p i18n/id/docusaurus-plugin-content-docs/current
cp -r docs/** i18n/id/docusaurus-plugin-content-docs/current
```

next version 3.7

```bash
npm run docusaurus docs:version 3.7
```

---
sidebar_position: 1
---

# Perkenalan

Selamat datang di YuukiPS, sebelum anda ingin terhubung ke server kami ada beberapa hal yang perlu anda ketahui.

## Dukungan Genshin Impact Private Server

* GIO 3.2 Tersedia untuk PC, Android, iOS
* GC 3.6 Tersedia untuk PC, Android, iOS

## Dukungan Star Rail Private Server

Mungkin saat rilis resmi nanti.

## Diperlukan Private Server Genshin Impact

### Komputer

* Patch RSAPatch untuk versi 3.3+
* Patch UserAssembly untuk versi 3.1-3.2
* Patch Metadata untuk versi 2.8-3.0

(Tips: Jangan dicampur, gunakan salah satunya saja.)

### Android & iOS

* Peluncur Proksi 1.0-2.7
* Proxy Launcher + modifikasi RSA + bypass SSL 2.8+

### Catatan

* Semua server gratis, jika Anda menggunakan server ini dengan membayar dari seseorang berarti Anda sedang ditipu jadi berhati-hatilah.
*Pastikan login minimal 1 bulan sekali, agar akun private server anda tidak terhapus dari server kami.
* Pemeliharaan server menghabiskan banyak uang. Jika Anda mampu membelinya, Anda dapat mempertimbangkan untuk mensponsori agar server tetap aktif dan semua orang akan senang.
